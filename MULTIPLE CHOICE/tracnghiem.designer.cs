﻿namespace bai2cs
{
    partial class tracnghiem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxfilepath = new System.Windows.Forms.TextBox();
            this.textBoxfilename = new System.Windows.Forms.TextBox();
            this.TextBoxNumOfCorrectAnswer = new System.Windows.Forms.TextBox();
            this.TextBoxNumOfWrongAnswer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DataGridViewAnswerTable = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.ButtonCheckAnswer = new System.Windows.Forms.Button();
            this.TextBoxContent = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewAnswerTable)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxfilepath
            // 
            this.textBoxfilepath.Location = new System.Drawing.Point(136, 37);
            this.textBoxfilepath.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxfilepath.Name = "textBoxfilepath";
            this.textBoxfilepath.Size = new System.Drawing.Size(251, 22);
            this.textBoxfilepath.TabIndex = 0;
            // 
            // textBoxfilename
            // 
            this.textBoxfilename.Location = new System.Drawing.Point(136, 82);
            this.textBoxfilename.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxfilename.Name = "textBoxfilename";
            this.textBoxfilename.Size = new System.Drawing.Size(251, 22);
            this.textBoxfilename.TabIndex = 1;
            // 
            // TextBoxNumOfCorrectAnswer
            // 
            this.TextBoxNumOfCorrectAnswer.Location = new System.Drawing.Point(136, 38);
            this.TextBoxNumOfCorrectAnswer.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxNumOfCorrectAnswer.Name = "TextBoxNumOfCorrectAnswer";
            this.TextBoxNumOfCorrectAnswer.Size = new System.Drawing.Size(251, 22);
            this.TextBoxNumOfCorrectAnswer.TabIndex = 2;
            // 
            // TextBoxNumOfWrongAnswer
            // 
            this.TextBoxNumOfWrongAnswer.Location = new System.Drawing.Point(136, 81);
            this.TextBoxNumOfWrongAnswer.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxNumOfWrongAnswer.Name = "TextBoxNumOfWrongAnswer";
            this.TextBoxNumOfWrongAnswer.Size = new System.Drawing.Size(251, 22);
            this.TextBoxNumOfWrongAnswer.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Đường dẫn file";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 86);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Tên file";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 42);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Số câu đúng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 85);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Số câu sai";
            // 
            // DataGridViewAnswerTable
            // 
            this.DataGridViewAnswerTable.AllowUserToOrderColumns = true;
            this.DataGridViewAnswerTable.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.DataGridViewAnswerTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewAnswerTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.DataGridViewAnswerTable.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.DataGridViewAnswerTable.Location = new System.Drawing.Point(16, 382);
            this.DataGridViewAnswerTable.Margin = new System.Windows.Forms.Padding(4);
            this.DataGridViewAnswerTable.Name = "DataGridViewAnswerTable";
            this.DataGridViewAnswerTable.RowHeadersWidth = 51;
            this.DataGridViewAnswerTable.Size = new System.Drawing.Size(456, 185);
            this.DataGridViewAnswerTable.TabIndex = 8;
            this.DataGridViewAnswerTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewAnswerTable_CellClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Question";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.Width = 125;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Answer";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.Width = 125;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Result";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.Width = 125;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(516, 11);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = "BẢNG GHI";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(173, 130);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 10;
            this.button1.Text = "Đọc file";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Read);
            // 
            // ButtonCheckAnswer
            // 
            this.ButtonCheckAnswer.Location = new System.Drawing.Point(173, 128);
            this.ButtonCheckAnswer.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonCheckAnswer.Name = "ButtonCheckAnswer";
            this.ButtonCheckAnswer.Size = new System.Drawing.Size(100, 28);
            this.ButtonCheckAnswer.TabIndex = 11;
            this.ButtonCheckAnswer.Text = "Xem đáp án";
            this.ButtonCheckAnswer.UseVisualStyleBackColor = true;
            this.ButtonCheckAnswer.Click += new System.EventHandler(this.ButtonCheckAnswer_Click);
            // 
            // TextBoxContent
            // 
            this.TextBoxContent.Location = new System.Drawing.Point(520, 34);
            this.TextBoxContent.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxContent.Name = "TextBoxContent";
            this.TextBoxContent.Size = new System.Drawing.Size(700, 542);
            this.TextBoxContent.TabIndex = 15;
            this.TextBoxContent.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxfilepath);
            this.groupBox1.Controls.Add(this.textBoxfilename);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(20, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(452, 166);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "NHẬP ĐỀ";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox2.Controls.Add(this.TextBoxNumOfCorrectAnswer);
            this.groupBox2.Controls.Add(this.ButtonCheckAnswer);
            this.groupBox2.Controls.Add(this.TextBoxNumOfWrongAnswer);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(20, 188);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(452, 175);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "KẾT QUẢ";
            // 
            // tracnghiem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1251, 604);
            this.Controls.Add(this.TextBoxContent);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DataGridViewAnswerTable);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "tracnghiem";
            this.Text = "tracnghiem";
            this.Load += new System.EventHandler(this.tracnghiem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewAnswerTable)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxfilepath;
        private System.Windows.Forms.TextBox textBoxfilename;
        private System.Windows.Forms.TextBox TextBoxNumOfCorrectAnswer;
        private System.Windows.Forms.TextBox TextBoxNumOfWrongAnswer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView DataGridViewAnswerTable;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button ButtonCheckAnswer;
        private System.Windows.Forms.RichTextBox TextBoxContent;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}